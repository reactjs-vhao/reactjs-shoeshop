import React from "react";
import ProductItem from "./ProductItem";

const ProductList = ({ productsData, setProductDetail }) => {
  return (
    <div className="row">
      {productsData.map((product) => (
        <ProductItem
          key={product.id}
          product={product}
          setProductDetail={setProductDetail}
        />
      ))}
    </div>
  );
};

export default ProductList;
