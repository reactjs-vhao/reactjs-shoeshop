import React from "react";

const MyModal = ({ content }) => {
  const { name, image, price, description, quantity } = content;
  return (
    <div
      className="modal fade"
      id="detailProdModal"
      tabIndex="-1"
      aria-labelledby="exampleModalLabel"
      aria-hidden="true"
    >
      <div className="modal-dialog">
        <div className="modal-content">
          <div className="modal-header">
            <h1 className="modal-title fs-5" id="exampleModalLabel">
              Thông tin chi tiết
            </h1>
            <button
              type="button"
              className="btn-close"
              data-bs-dismiss="modal"
              aria-label="Close"
            ></button>
          </div>
          <div className="modal-body">
            <div className="row">
              <div className="col-4">
                <img src={image} className="w-100" alt="" />
              </div>
              <div className="col-8">
                <p className="mb-0">
                  <b>Sản phẩm:</b> {name}
                </p>
                <p className="mb-0">
                  <b>Giá:</b> {price} $
                </p>
                <p className="mb-0">
                  <b>Số lượng:</b> {quantity} cái
                </p>
                <p className="mb-0">
                  <b>Mô tả:</b> {description}
                </p>
              </div>
            </div>
          </div>
          <div className="modal-footer">
            <button
              type="button"
              className="btn btn-secondary"
              data-bs-dismiss="modal"
            >
              Đóng
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default MyModal;
