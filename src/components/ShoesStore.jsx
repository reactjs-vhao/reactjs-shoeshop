import React, { useState } from "react";
import products from "../data/data.json";
import ProductList from "./ProductList";
import Modal from "./Modal";

const ShoesStore = () => {
  const [productDetail, setProductDetail] = useState(products[0]);
  return (
    <div className="container mb-5">
      <h1 className="text-center">Shoes shop</h1>
      <ProductList
        productsData={products}
        setProductDetail={setProductDetail}
      />
      <Modal content={productDetail} />
    </div>
  );
};

export default ShoesStore;
