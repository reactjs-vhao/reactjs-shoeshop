import React from "react";
const ProductItem = ({ product, setProductDetail }) => {
  return (
    <div className="col-4 mt-3">
      <div className="card">
        <img
          className="card-img-top"
          style={{ height: "260px", width: "300px" }}
          src={product.image}
          alt=""
        />
        <div className="card-body pt-0">
          <h4 className="card-title">{product.name}</h4>
          <p className="card-text mb-0">{product.price} $</p>
          <button
            className="btn btn-outline-success rounded-0"
            data-bs-toggle="modal"
            data-bs-target="#detailProdModal"
            onClick={() => {
              setProductDetail(product);
            }}
          >
            Xem chi tiết
          </button>
        </div>
      </div>
    </div>
  );
};

export default ProductItem;
